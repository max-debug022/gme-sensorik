import unittest
from modules.ConfigLoader import ConfigLoader


class MyTestCase(unittest.TestCase):
    def test_path(self):
        path: str = ConfigLoader.get_relativ_path()
        self.assertEqual(path, "..\\modules\\ConfigLoader.py")

    def test_parameter(self):
        cl = ConfigLoader()

        section = "CONFIG_LOADER"
        parameter = ["TEST_PARAMETER_BOOL", "TEST_PARAMETER_STR", "test_PARAMETER_INT"]

        b = bool(cl.get_option(section, parameter[0]))
        self.assertEqual(b, True)

        s = str(cl.get_option(section, parameter[1]))
        self.assertEqual(s, "Testparameter")

        i = int(cl.get_option(section, parameter[2]))
        self.assertGreaterEqual(i, 1)


if __name__ == '__main__':
    unittest.main(verbosity=3)
