CREATE TABLE `ultraschall` (
	`timestamp` TIMESTAMP(20) ON UPDATE CURRENT_TIMESTAMP,
	`sensor` TEXT(20),
	`entfernung` INT(20),
	`id` INT(20),
	`fehler` BOOLEAN(20),
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;