CREATE TABLE `temperatur` (
	`timestamp` TIMESTAMP(20) ON UPDATE CURRENT_TIMESTAMP,
	`name` TEXT(20),
	`value` INT(20),
	`id` INT(20),
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;