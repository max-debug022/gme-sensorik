from flask import Flask, send_file

from src.core.ConfigLoader import ConfigLoader
from src.api.DataInjector import DataInjector
from src.core.Logger import Logger


class Server:
    _application_: Flask
    _logger_: Logger
    _cl_: ConfigLoader

    _host_: str
    _port_: int

    def __init__(self, import_name):
        self._cl_ = ConfigLoader()
        self._logger_ = Logger("Server")

        self._host_ = self._cl_.get_option("SERVER", "HOST", str)
        self._port_ = self._cl_.get_option("SERVER", "PORT", int)

        self._application_ = Flask(import_name)

        self._logger_.log("Server initialized.")

    def __del__(self):
        self._logger_.log("Server closed.")

    def start(self):
        # Website verknüpfen
        self._application_.add_url_rule("/", view_func=self._index_)
        # self._application_.add_url_rule("/favicon.ico", view_func=self._favicon_)

        self._logger_.log("Server started.")
        self._application_.run(host=self._host_, port=self._port_)

    def _index_(self):
        page: str = "ERROR"

        DataInjector.inject("", "./templates/index.html", "")

        with open("./templates/index.html", "r") as file:
            page = file.read()
            self._logger_.log("Index loaded.")

        return page

    def _favicon_(self):
        self._logger_.log("Favicon loaded.")
        return send_file("../../static/img/favicon.ico")
