from json import loads


class Protocoller:
    @staticmethod
    def convert_data(data: str) -> [str, str, str]:
        output: dict = loads(data)

        temperature = output.get("temperature")
        humidity = output.get("humidity")
        pressure = output.get("pressure")

        return [temperature, humidity, pressure]
