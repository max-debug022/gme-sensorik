from serial import Serial

from src.core.ConfigLoader import ConfigLoader
from src.core.Logger import Logger


class SerialBus:
    _serial_: Serial
    _logger_: Logger
    _cl_: ConfigLoader

    def __init__(self):
        self._logger_ = Logger("SerialBus")
        self._cl_ = ConfigLoader()

        port: str = self._cl_.get_option("SERIAL", "PORT", str)
        baud: int = self._cl_.get_option("SERIAL", "BAUD", int)
        timeout: int = self._cl_.get_option("SERIAL", "TIMEOUT", int)

        self._serial_ = Serial(port, baud, timeout=timeout)
        self._logger_.log(self._serial_.name + " successfully initialized!")

    def __enter__(self):
        pass

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._serial_.close()
        self._logger_.log("SerialBus disconnected.")

    def __del__(self):
        self._serial_.close()
        self._logger_.log("SerialBus disconnected.")

    def capture(self) -> str:
        data = self._serial_.readline().decode("utf-8")

        self._logger_.log(str(data))
        return data
