class DataInjector:
    @staticmethod
    def inject(data_file_path: str, target_file_path: str, placeholder: str):
        with open(data_file_path, "r") as file:
            data = file.read()

        with open(target_file_path, "r") as file:
            target_data = file.read().replace(placeholder, data)

        with open(target_file_path, "w") as file:
            file.write(target_data)
