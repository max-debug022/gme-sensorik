int trigger=9;                               // Der Trigger Pin
int echo=8;                                  // Der Echo Pin
long dauer=0;                                // Hier wird die Zeitdauer abgespeichert
long entfernung=0;                           // Hier wird die Entfernung vom


void setup()
{
    Serial.begin(9600);                      // Die serielle Kommunikation starten

    pinMode(trigger, OUTPUT);                // Trigger Pin als Ausgang definieren

    pinMode(echo, INPUT);                    // Echo Pin als Eingang defnieren
}



void loop()
{
    digitalWrite(trigger, LOW);              // Den Trigger auf LOW setzen um
    delay(5);                                // 5 Millisekunden warten

    digitalWrite(trigger, HIGH);             // Den Trigger auf HIGH setzen um eine
    delay(10);                               // 10 Millisekunden warten

    digitalWrite(trigger, LOW);              // Trigger auf LOW setzen um das
    dauer = pulseIn(echo, HIGH);             // Die Zeit messen bis die

    entfernung = (dauer/2) / 29.1;           // Die Zeit in den Weg in Zentimeter umrechnen

        Serial.print(entfernung);            // Den Weg in Zentimeter ausgeben

        Serial.println(" cm");               //

    delay(1000);                             // Nach einer Sekunde wiederholen
}