// =========================================
// Von Maximilian Pelka
//TEST File für Buzzer
// =========================================


int tmp36_pin = A1;
int red_led = 12;
int green_led = 8;
int buzzer_alarm = 2;

void setup()
{
  pinMode(tmp36_pin, INPUT);
  pinMode(red_led, OUTPUT);
  pinMode(green_led, OUTPUT);
  pinMode(buzzer_alarm, OUTPUT);
  digitalWrite(red_led, HIGH);
}
void loop()
{
  float tmp36_value = analogRead(tmp36_pin);
  float analog_ = (tmp36_value / 1023) * 5000;
  float digital_ = analog_ / 10.0;
  if (digital_ > 65)
  {
    tone(buzzer_alarm, 600);
    digitalWrite(green_led, HIGH);
    digitalWrite(red_led, LOW);
  }
  else
  {
    noTone(buzzer_alarm);
    digitalWrite(red_led, HIGH);
    digitalWrite(green_led, LOW);
  }
}