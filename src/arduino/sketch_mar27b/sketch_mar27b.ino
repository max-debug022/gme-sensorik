int STATUS_LED = 12;
 
void setup() 
{   
  Serial.begin(9600);

  pinMode(STATUS_LED, OUTPUT); 
} 
void loop() 
{ 
  digitalWrite(STATUS_LED, HIGH);
  Serial.write("{\"temperature\": 21, \"humidity\": 30, \"pressure\": 1080}");
  delay(1000);
} 
