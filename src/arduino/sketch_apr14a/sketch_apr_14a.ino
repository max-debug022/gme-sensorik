// Make a lcd screen display temperature in fahrenheit with a temperature sensor!

#include <LiquidCrystal.h>

LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

int sensorPin = A0;
int counter = 0;
float tempsum = 0;
float lightsum = 0;

void setup()
{
  lcd.begin(16, 2);
  pinMode(7, OUTPUT);
  pinMode(6, INPUT);
  Serial.begin(9600);
}

void loop()
{

int reading = analogRead(sensorPin);
float voltage = reading * 5.0;
voltage /= 1024.0;

float temperatureC = (voltage - 0.5) * 100 ;
tempsum += temperatureC;

lightsum += digitalRead(6);
Serial.println(String(lightsum));

  if(lightsum > 5){
    digitalWrite(7, HIGH);
  }


  if(counter % 10 == 0){
    lcd.clear();
    lcd.println("Temp ");

    temperatureC = tempsum / 10;
    lcd.print(temperatureC);
    tempsum = 0;
    counter = 0;

    lcd.println(" C ");
  }
  counter ++;

delay(100);

}