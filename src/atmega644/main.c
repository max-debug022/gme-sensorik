/*
 * main.c
 *
 * Created: 03.04.2022 11:02:03
 * Author : Maximilian Pelka
 */
#define F_CPU 16000000

#include <avr/io.h>
#include <util/delay.h>

void USART_Init(void)
{
	/* Set baud rate */
	// F_CPU 16MHz
	UBRR0H = 0;
	UBRR0L = 103;
	/* Enable receiver and transmitter */
	UCSR0B = (1<<RXEN0)|(1<<TXEN0);
	/* Set frame format: 8data, 2stop bit */
	UCSR0C = (1<<UCSZ00)|(1<<UCSZ01);
}

void USART_Transmit(int i)
{
	//Wait for empty transmit buffer
	//wartet 1,04ms ms um Daten zu senden
	while(!(UCSR0A & (1<<UDRE0)));
	//Put data into buffer, sends the data
	UDR0 = i;
}

int main(void)
{
    USART_Init();
	int zahl = 5 + 0x30;

    while (1)
    {
		USART_Transmit(zahl);

		_delay_ms(200.0);
    }
}

