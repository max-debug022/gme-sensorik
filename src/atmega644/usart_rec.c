/*
 * usart.c
 *
 * Created: 02.04.2022 08:24:38
 *  Author: Maximilian Pelka
 */

#include <avr/io.h>
#include "usart.h"
#include <avr/interrupt.h>

//Globals
char data[3]; //allocate Array Size
char *pData;

void USART_Init(void)
{
	/* Set baud rate */
	// F_CPU 16MHz
	pData = data;
	UBRR0H = 0;
	UBRR0L = 103;
	/* Enable receiver and transmitter */
	UCSR0B = (1<<RXEN0)|(1<<TXEN0)|(1<<RXCIE0);//enable transmitting (1<<RXCIE0)=Recieve interrupt enable
	/* Set frame format: 8data, 2stop bit */
	UCSR0C = (1<<UCSZ00)|(1<<UCSZ01);
}


ISR(USART0_RX_vect)
{
	*pData = UDR0; 	//recieve data
	UDR0 = *pData;	//send data
	pData++;		//increase pointer

	//test if array contains "AUS" or "EIN"
	if((*(pData-1) == 'S') && (*(pData-2) == 'U') && (*(pData-3) == 'A'))
	{
		PORTD |= (1<<PD7);
		pData=pData-3;
	}
	else if((*(pData-1) == 'N') && (*(pData-2) == 'I' ) && (*(pData-3) == 'E'))
	{
		PORTD &= ~(1<<PD7);
		pData=pData-3;
	}
};