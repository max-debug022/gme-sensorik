# ======================================================================================================================
# Source Code by: Maximilian Pelka
# ======================================================================================================================

from os import path
from datetime import datetime

from src.core.ConfigLoader import ConfigLoader


class Logger:
    _cl_: ConfigLoader

    _debug_: bool
    _log_: bool
    _timestamp_: bool

    _log_file_: str
    _tag_: str

    _exclude_: list

    def __init__(self, tag: str):
        self._cl_ = ConfigLoader()
        self._tag_ = tag.upper()

        # Define Log file
        rel_path_root = path.relpath(__file__).replace("Logger.py", "") + "../../"
        self._log_file_ = rel_path_root + self._cl_.get_option("LOGGER", "LOG_FILE")

        # Define bools
        self._debug_ = bool(self._cl_.get_option("LOGGER", "DEBUG"))
        self._log_ = bool(self._cl_.get_option("LOGGER", "LOG"))
        self._timestamp_ = bool(self._cl_.get_option("LOGGER", "TIMESTAMP"))

        self._load_excludes_()

    def _load_excludes_(self):
        # Update ConfigLoader
        self._cl_.reload()
        # Define excludes
        self._exclude_ = self._cl_.get_option("LOGGER", "EXCLUDE").split(", ")
        if len(self._exclude_) == 0:
            self._exclude_ = [self._cl_.get_option("LOGGER", "EXCLUDE")]

    def log(self, message: str, override_tag: str = None):
        self._load_excludes_()

        tag = self._tag_
        message = message.replace("\n", " ")
        message = message.replace("\r", " ")

        if override_tag is not None:
            tag = override_tag

        if self._exclude_.__contains__(tag):
            return

        message = "[" + tag + "] " + message

        if self._timestamp_:
            message = "[" + datetime.now().strftime('%Y-%m-%d %H:%M:%S') + "] " + message

        if self._log_:
            with open(self._log_file_, "a") as file:
                file.write(message + "\n")

        if self._debug_:
            message = "[LOG]" + message
            print(message)




