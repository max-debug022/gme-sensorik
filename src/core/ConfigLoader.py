# ======================================================================================================================
# Source Code by: Maximilian Pelka
# ======================================================================================================================

from configparser import ConfigParser
from os import path


class ConfigLoader:
    _parser_: ConfigParser
    _config_path_: str

    def __init__(self, config_path: str = None):
        if config_path is None:
            config_path = path.relpath(__file__) + "../../data/CONFIG.ini"
            config_path = config_path.replace("ConfigLoader.py", "")

        self._parser_ = ConfigParser()
        self._parser_.read(config_path)
        self._config_path_ = config_path

    def get_option(self, section: str, parameter: str, return_type: type = str):
        section = section.upper()
        parameter = parameter.upper()

        return return_type(self._parser_.get(section, parameter))

    def reload(self) -> None:
        self._parser_.read(self._config_path_)

    @staticmethod
    def get_relativ_path() -> str:
        return path.relpath(__file__)
