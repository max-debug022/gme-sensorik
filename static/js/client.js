export default class Client {
    constructor(host="localhost", port=42069) {
        this.ups = 80; // Updates per second
        this.updater = 0;

        this.id = 0;

        this.#initSocket(host, port);
    }

    run() {
        this.send(COMMANDS);
        this.#initKeyListeners();
        this.#initUpdater();
    }

    #initSocket(host, port) {
        let self = this

        this.socket = new WebSocket("ws://"+host+":"+String(port));

        this.socket.onopen = function () {
            self.#log("Connection established!");
        }
        this.socket.onclose = function (event) {
            this.updater = 0;
            if (event.wasClean) {
                self.#log("Connection closed!");
            }
            else {
                self.#log("Connection died!");
            }
        }
        this.socket.onerror = function (event) {
            this.updater = 0;
            self.#log(event.message);
        }

        this.socket.onmessage = function (event) {
            self.receive(event.data);
        };
    }

    #initKeyListeners() {
        let self = this;

        document.addEventListener("keydown", function (event) {
            if (event.code === self.KEY_UP) {
                self.send(S_COMMANDS, [0, -1]);
            } else if (event.code === self.KEY_A) {
                self.send(S_COMMANDS, [-1, 0]);
            } else if (event.code === self.KEY_G) {
                self.send(S_COMMANDS, [0, 1]);
            } else if (event.code === self.KEY_H) {
                self.send(S_COMMANDS, [1, 0]);
            }
            else {
                self.send(S_COMMANDS.UPDATE)
            }
        });
    }

    #initUpdater() {
        let self = this;
        this.updater = setInterval(function () {
            if(self.player_id !== 0){
                self.send(S_COMMANDS.UPDATE);
            }
        }, 1000/self.ups);
    }

    send(command, data){
        let message = ""
        if(command === S_COMMANDS){
            // TODO: Init name
            message = '{"command": "'+S_COMMANDS+'", "data": {}}';
            this.#log("Command sent: " + command);
        }
        else if(command === S_COMMANDS){
            this.#log("Command sent: " + command);
        }
        else if(command === S_COMMANDS.UPDATE){
            if((this.updater === 0) || this.socket.readyState === WebSocket.CLOSED){
                return;
            }

            message = '{"command": "'+S_COMMANDS.UPDATE+'", "data":{"id": '+this.game_id+'}}';
        }
        this.socket.send(message);
    }

    #log(message) {
        if(DEBUG){
            console.log("[LOG][SOCKET] " + message);
        }
    }
}